<?php
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    #return $router->app->version();
    return "Hola";
});

$router->get('/call', ['uses'=>'CallsController@index']);

$router->get('/key', function () {
    #return $router->app->version();
   return Str::random(32);
});